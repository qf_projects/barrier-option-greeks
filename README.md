This project is about Barrier options greeks and pricing.

Due to confidentiality restrictions, I cannot post the full code since this work has been done for an employer.

Nevertheless, I am droping off some charts to give you an idea on what this is :

This tool can calculate the greeks for barrier options, for any strike, maturity, spot etc ..

It also compares them to vanilla options greeks , and finally, can compare the greeks across different maturities.

For further informations , please contact me at my e-mail : melek.benelghoul@ept.ucar.tn

Enjoy !
